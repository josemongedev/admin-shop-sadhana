import { RootState } from "state";

export const selectOrdersState = (state: RootState) => state.orders;
export const selectOrders = (state: RootState) => state.orders.orders;
export const selectOrderById = (orderId: string) => (state: RootState) =>
  state.orders.orders.find((order) => orderId === order._id);
