import { RootState } from "state";

export const selectProductState = (state: RootState) => state.product;
export const selectProducts = (state: RootState) => state.product.products;
export const selectProductById = (productId: string) => (state: RootState) =>
  state.product.products.find((product) => product._id === productId);
