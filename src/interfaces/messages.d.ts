interface IUIMessage {
  text: string;
  color: "success" | "error" | "info";
}
