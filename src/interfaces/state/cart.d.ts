import { IProduct } from "interfaces";

export interface cartItemPayload extends Omit<IProduct, "color" | "size"> {
  price: number;
  quantity: number;
  color: string;
  size: string;
}

export interface ICartState {
  products: cartItemPayload[];
  quantity: number;
  total: number;
}
