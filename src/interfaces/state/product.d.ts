export type TGetProductPayload = IProduct[];
export type TDeleteProductPayload = { id: string };
export type TUpdateProductPayload = { product: IProduct } & { id: string };
export interface IAddProductPayload {
  product: IProduct;
}

export interface IProductState {
  products: IProduct[];
  isFetching: boolean;
  error: boolean;
}
