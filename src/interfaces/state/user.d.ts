import { IUser } from "interfaces";

export type TUserPayload = IUser;
export type TGetUsersListPayload = IUser[];
export type TUpdateUsersListPayload = IUser;
export type TRegisterUserPayload = IUser;
export type TDeleteUserPayload = { id: string };
export type TUpdateUserPayload = { user: IUser } & { id: string };
export type TAddUserPayload = { user: IUser };

type TCheckStatus = "neverLogged" | "loggedIn" | "loggedOut";

export interface IUserState {
  currentUser: IUser | null;
  usersList: IUser[];
  userStatus: TCheckStatus;
  isFetching: boolean;
  error: boolean;
}
