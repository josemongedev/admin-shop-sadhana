export interface IListElement {
  link: string;
  icon: JSX.Element;
  tag: string;
  active: boolean;
}
export interface IListOption {
  header: string;
  list: IListElement[];
}
