// type TNewUser = Omit<IUser, "_id" | "createdAt">;
export interface INewUser {
  username: string;
  fullname: string;
  address: string;
  phone: string;
  email: string;
  password: string;
  isAdmin: boolean;
  active: boolean;
  img: string;
}

export interface IUser extends INewUser {
  _id: string;
  createdAt: string;
}

interface IUserStats {
  _id: number;
  total: number;
}
