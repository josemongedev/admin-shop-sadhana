export interface IIndicatorData {
  quantity: number;
  trend: number;
  timeframe: string;
}
export type TIndicatorObject = { name: string; data: IIndicatorData };

export interface IMainPanelIndicators {
  Revenue: IIndicatorData;
  Cost: IIndicatorData;
  Sales: IIndicatorData;
}
