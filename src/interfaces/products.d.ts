import { TColors, TSizes } from "./types/products";

interface IFilters {
  color?: TColors;
  size?: TSizes;
}

interface INewProduct {
  title: string;
  desc: string;
  price: number;
  inStock: string;
  active: string;
  categories: string[];
  img: string;
  size: string[];
  color: string[];
}

interface IProduct extends INewProduct {
  [key: string]: string | string[] | boolean | number;
  _id: string;
  createdAt: string;
  updatedAt: string;
  // categories: string[];
  // size: string[];
  // color: string[];
  // desc: string;
  // img: string;
  // inStock: boolean;
  // price: number;
  // size: string[];
  // title: string;
}
