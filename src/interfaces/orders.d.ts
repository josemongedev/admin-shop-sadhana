import { Item } from "@/interfaces";

interface IOrder {
  _id: string;
  userId: string;
  products: Item[];
  amount: number;
  address: string;
  status: string;
  createdAt: any;
}

interface IIncome {
  _id: number;
  total: number;
}

interface IOrderState {
  orders: IOrder[];
  isFetching: boolean;
  error: boolean;
}
