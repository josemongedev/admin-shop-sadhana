// export const apiServerHost = "http://localhost:5000/api";
export const apiServerHost = "https://api-sadhana-shop.herokuapp.com/api";

export const defaultHomeAuthRoute = "/login";

// UI Config
export const DRAWER_WIDTH = 270;
