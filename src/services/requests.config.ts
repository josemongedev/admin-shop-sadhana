import axios, { AxiosRequestConfig } from "axios";

const baseConfig: AxiosRequestConfig<any> = {
  // baseURL: apiServerHost,
};

const authConfig: AxiosRequestConfig<any> = {
  withCredentials: true,
};

export const publicRequest = axios.create(baseConfig);

export const authRequest = axios.create(authConfig);
