// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDyheQxY6yC48-WZ2iu4Z5fd-ld4DfP4Kk",
  authDomain: "sadhana-104af.firebaseapp.com",
  projectId: "sadhana-104af",
  storageBucket: "sadhana-104af.appspot.com",
  messagingSenderId: "730604093692",
  appId: "1:730604093692:web:f4e60be914224b0fd31779",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;
