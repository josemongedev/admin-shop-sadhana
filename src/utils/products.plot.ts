import { EMonths } from "constant";

export interface IProductStats {
  name: EMonths;
  sales: number;
}

export const initialProductStats: IProductStats[] = [
  { name: EMonths.Jan, sales: 0 },
  { name: EMonths.Feb, sales: 0 },
];
